﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(RestorerTest.Startup))]
namespace RestorerTest
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
