﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace RestorerTest.Models 
{
    public class Customers 
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Telephone { get; set; }
        public virtual ICollection<Inventory> Inventory { get; set; }
    }
}