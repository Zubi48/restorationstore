﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace RestorerTest.Models
{
    public class Investments 
    {
        public int Id { get; set; }
        public double Concept { get; set; }
        public string Description { get; set; }
        public virtual ICollection<Inventory> Inventory { get; set; }
    }
}