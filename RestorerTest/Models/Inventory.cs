﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace RestorerTest.Models
{
    public class Inventory 
    {
        public int Id{ get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        [ForeignKey("Customer")]
        [Display(Name = "Customer")]
        public int Cust { get; set; }
        [ForeignKey("Investment")]
        [Display(Name = "Investment")]
        public int Invest { get; set; }

        public Customers Customer { get; set; }
        public Investments Investment { get; set; }

    }
}